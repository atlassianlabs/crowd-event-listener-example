package com.atlassian.crowd.event.listener.example;

import com.atlassian.crowd.event.user.UserCreatedEvent;

public class UserCreatedListener {

    @com.atlassian.event.api.EventListener
    public void printUserCreatedEvent(UserCreatedEvent event) {
        System.out.println("User " + event.getUser().getDisplayName() + " has been created.");
    }
    
}