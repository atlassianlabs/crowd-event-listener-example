# Crowd Event Listener Example

[![Atlassian license](https://img.shields.io/badge/license-Apache%202.0-blue.svg?style=flat-square)](LICENSE) [![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](CONTRIBUTING.md)

This is an example of the Crowd Event Listener plugin that logs information when `UserCreatedEvent` occurs.

## Installation

```shell
mvn clean package
```

## Usage

If you have a Crowd instance running, in Crowd go to Administration > Manage apps and upload the `.jar` file which can
be found in the `target/` directory.

Alternatively, you can use `mvn crowd:debug` to start a new Crowd instance with the freshly built version of the plugin.

## Documentation

https://developer.atlassian.com/server/crowd/event-listeners/

## Contributions

Contributions to Crowd Event Listener Example are welcome! Please see [CONTRIBUTING.md](CONTRIBUTING.md) for details.

## License

Copyright (c) 2011 - 2021 Atlassian and others. Apache 2.0 licensed, see [LICENSE](LICENSE) file.

[![With ❤️ from Atlassian](https://raw.githubusercontent.com/atlassian-internal/oss-assets/master/banner-cheers-light.png)](https://www.atlassian.com)
